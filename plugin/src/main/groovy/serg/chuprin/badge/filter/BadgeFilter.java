package serg.chuprin.badge.filter;

import java.awt.image.BufferedImage;

public interface BadgeFilter {

    void apply(BufferedImage image);
}
