package serg.chuprin.badge.sample

import android.app.Activity
import android.os.Bundle
import serg.chuprin.badge.R

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
